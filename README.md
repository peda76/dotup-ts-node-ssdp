# [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]

# dotup-ts-node-ssdp

## Description

## Installation

## Configuration

## Usage

## Release Notes
### 1.0.0

Fixes/Features:
- Initial release

## License

MIT © [Peter Ullrich](https://github.com/dotupNET/)

**Enjoy!**

[npm-image]: https://badge.fury.io/js/dotup-ts-node-ssdp.svg
[npm-url]: https://npmjs.org/package/dotup-ts-node-ssdp
[travis-image]: https://travis-ci.org/dotupNET/dotup-ts-node-ssdp.svg?branch=master
[travis-url]: https://travis-ci.org/dotupNET/dotup-ts-node-ssdp
[daviddm-image]: https://david-dm.org/dotupNET/dotup-ts-node-ssdp.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/dotupNET/dotup-ts-node-ssdp
[coveralls-image]: https://coveralls.io/repos/dotupNET/dotup-ts-node-ssdp/badge.svg
[coveralls-url]: https://coveralls.io/r/dotupNET/dotup-ts-node-ssdp
