import * as os from 'os';
import events = require('events');
import { getLogger, Logger } from 'log4js';

export const LOGGER_NAME = 'SSDP';

export function getNamedLogger(name?: string): Logger {
  return getLogger(name);
  // if (process.env.NODE_ENV === 'debug') {
  //   return getLogger('debug');
  // } else if (name === undefined) {
  //   return getLogger(LOGGER_NAME);
  // } else {
  //   return getLogger(name);
  // }
}

// tslint:disable-next-line: no-any: interface-name
export interface StringProperty { [key: string]: any; }

export function getOsAndVersion(): string {
  return `${os.platform}/${os.release()}`;
}

export function getHostname(): string {
  return os.hostname();
}
