// http://www.upnp.org/specs/arch/UPnP-arch-DeviceArchitecture-v1.1.pdf
export enum SsdpControlPointEvents {
  found = 'alive',
  lost = 'byebye'
}
