// ARP Address Resolution Protocol SOAP Simple Object Access Protocol
// CP Control Point SSDP Simple Service Discovery Protocol
// DCP Device Control Protocol UDA UPnP Device Architecture
// DDD Device Description Document UPC Universal Product Code
// DHCP Dynamic Host Configuration Protocol URI Uniform Resource Identifier
// DNS Domain Name System URL Uniform Resource Locator
// GENA General Event Notification Architecture URN Uniform Resource Name
// HTML Hypertext Markup Language UUID Universally Unique Identifier
// HTTP Hypertext Trrdansfer Protocol XML Extensible Markup Language
// SCPD Service Control Protocol Description
// SOAP Simple Object Access Protocol
// SSDP Simple Service Discovery Protocol
// UDA UPnP Device Architecture
// UPC Universal Product Code
// URI Uniform Resource Identifier
// URL Uniform Resource Locator
// URN Uniform Resource Name
// UUID Universally Unique Identifier
// XML Extensible Markup Language
export enum SsdpSocketEvents {
  listening = 'listening',
  message = 'message',
  error = 'error',
  notify = 'notify',
  msearch = 'msearch'
}
