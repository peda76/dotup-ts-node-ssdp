import { Socket } from 'dgram';

export class DgramSocket extends Socket {
  interfaceName: string;
  ip: string;
  connected: boolean;
}
