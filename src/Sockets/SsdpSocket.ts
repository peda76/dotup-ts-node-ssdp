import { createSocket, RemoteInfo, SocketOptions } from 'dgram';
import { IDisposable, StringTools } from 'dotup-ts-types';
import { EventEmitter } from 'events';
import os, { NetworkInterfaceInfo } from 'os';
import { SsdpMessage } from '../Messages/SsdpMessage';
import { SsdpSearchMessage } from '../Messages/SsdpSearchMessage';
import { SsdpConstants } from '../SsdpConstants';
import { getHostname, getNamedLogger, LOGGER_NAME } from '../tools';
import { DgramSocket } from './DgramSocket';
import { SsdpSocketEvents } from './SsdpSocketEvents';

const logger = getNamedLogger(LOGGER_NAME);

export class SsdpSocket extends EventEmitter implements IDisposable {
  protected sockets: DgramSocket[];

  readonly interfaceNames: string[];

  constructor(...interfaceNames: string[]) {
    super();
    this.interfaceNames = interfaceNames.length > 0 ? interfaceNames : undefined;
  }

  publish(message: string): void {
    this.sockets.forEach(s => {
      const data = StringTools.format(message, { ip: s.ip, hostname: getHostname() });
      s.send(data, SsdpConstants.port, SsdpConstants.ipv4Address, (e, b) => {
        if (e !== undefined && e !== null) {
          logger.error(e);
        }
      });
    });
  }

  isConnected(): boolean {
    if (this.sockets === undefined || this.sockets.length < 0) {
      return false;
    }
    return this.sockets.every(s => s.connected);
  }

  initialize(): void {
    const interfaces = os.networkInterfaces();
    const allKeys = Object.keys(interfaces);
    let keys: string[] = allKeys;

    if (this.sockets === undefined) {
      this.sockets = [];
    } else {
      logger.warn('Already initialized');
      return;
    }

    if (this.interfaceNames !== undefined) {
      const notFound = this.interfaceNames.filter(name => interfaces[name] === undefined);
      notFound.forEach(name => logger.warn(`Interface '${name}' not found.`));

      keys = allKeys.filter(name => this.interfaceNames.includes(name));
    }

    if (!this.hasIp4(keys)) {
      logger.fatal('No ipv4 interface available');
      process.exit(8001);
    }

    keys.forEach(name => {
      this.createSocket(name, interfaces[name]);
    });

    setTimeout(() => {
      if (!this.isConnected()) {
        logger.warn('Socket bind failed.');
        this.dispose();
        this.initialize();
      }
    }, 5000);
  }

  private hasIp4(keys: string[]): boolean {
    const interfaces = os.networkInterfaces();

    return keys.some(key => {
      const netInfo = interfaces[key];

      return netInfo.some(net => net.family === 'IPv4');
    });
  }

  private createSocket(name: string, netInfo: NetworkInterfaceInfo[]): void {

    netInfo.forEach(info => {
      logger.info(`netInfo:${JSON.stringify(info)}`);

      if (info.internal || info.family === 'IPv6') {
        return;
      }

      const opt: SocketOptions = {
        type: 'udp4',
        reuseAddr: true
      };

      logger.info(`Try create socket ${name}(${info.address})`);
      const socket = <DgramSocket>createSocket(opt);
      socket.ip = info.address;
      socket.interfaceName = name;
      socket.connected = false;

      logger.info(`Socket ${name}(${socket.ip}) created`);

      socket.on('message', (m, r) => {
        this.emit(SsdpSocketEvents.message, m, r);
        this.onMessage(m, r);
      });

      socket.on('error', e => {
        logger.error(e);
        this.emit(SsdpSocketEvents.error, e);
      });

      socket.on('listening', () => {
        socket.connected = true;
        this.emit(SsdpSocketEvents.listening, socket);
        logger.info(`Socket ${socket.ip} listening`);
      });

      socket.bind(SsdpConstants.port, () => {
        socket.addMembership(SsdpConstants.ipv4Address, info.address);
        socket.setMulticastTTL(SsdpConstants.ttl);
        socket.setMulticastLoopback(true);
        socket.setBroadcast(true);
        logger.info(`Socket ${socket.ip} configured`);
      });

      this.sockets.push(socket);

    });

  }

  private onMessage(msg: Buffer, rinfo: RemoteInfo): void {
    logger.debug(msg.toString());

    const rows = msg
      .toString()
      .split('\r\n');
    const m = new SsdpMessage();
    // tslint:disable-next-line: no-any
    const startLine = <any>rows.shift();

    rows.forEach(row => {
      if (row.trim().length > 0) {
        const split = row.indexOf(':');
        const key = row
          .substr(0, split)
          .trim()
          .toUpperCase();
        const value = row
          .substr(split + 1)
          .trim();
        // tslint:disable-next-line: no-any
        (<any>m)[key] = value;
      }
    });

    switch (startLine) {
      case SsdpConstants.StartLine['M-SEARCH']:
        this.emit(SsdpSocketEvents.msearch, m, rinfo);
        break;

      case SsdpConstants.StartLine.NOTIFY:
        this.emit(SsdpSocketEvents.notify, m);
    }
  }

  // tslint:disable: no-unnecessary-override
  on(event: SsdpSocketEvents.listening, listener: () => void): this;
  on(event: SsdpSocketEvents.error, listener: (error: Error) => void): this;
  on(event: SsdpSocketEvents.message, listener: (message: string) => void): this;
  on<T extends SsdpMessage>(event: SsdpSocketEvents.notify, listener: (message: T) => void): this;
  on<T extends SsdpSearchMessage>(event: SsdpSocketEvents.msearch, listener: (message: T, rinfo: RemoteInfo) => void): this;
  // tslint:disable-next-line: no-any
  on(event: string | symbol, listener: (...args: any[]) => void): this {
    return super.on(event, listener);
  }

  dispose(): void {
    if (this.sockets !== undefined) {
      this.sockets.forEach(s => {
        s.removeAllListeners();
        s.close();
        s.unref();
      });
      this.sockets = undefined;
    }
  }

}
