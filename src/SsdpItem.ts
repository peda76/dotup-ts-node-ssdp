import { SsdpType } from './Descriptors/SsdpType';

export interface SsdpItem {
  timestamp: string;
  type: SsdpType;
  domain: string;
  name: string;
  uuid: string;
  validitySEC: Number;
  USN: string;
}
