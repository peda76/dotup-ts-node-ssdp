import { SsdpConfig } from './SsdpConfig';
import { SsdpRootDeviceDescriptor } from './Descriptors/SsdpRootDeviceDescriptor';
import { SsdpServiceDescriptor } from './Descriptors/SsdpServiceDescriptor';
import { getHostname } from './tools';

export namespace SsdpConfigLoader {
  export function createRootDeviceDescriptor(config: SsdpConfig): SsdpRootDeviceDescriptor {
    const rootConfig = config.SsdpAdvertiser.RootDevice;

    // Create root device
    const root = new SsdpRootDeviceDescriptor(rootConfig.domain, rootConfig.name, getHostname());

    rootConfig.services.forEach(serviceConfig => {
      const service = new SsdpServiceDescriptor(serviceConfig.name);

      const headers = Object.keys(serviceConfig.headers);

      headers.forEach(header => {
        service.addHeader(header, serviceConfig.headers[header]);
      });

      // Add service
      root.addService(service);

    });

    return root;
  }
}
