import { IDisposable, sleep } from 'dotup-ts-types';
import { clearTimeout } from 'timers';
import { SsdpDescriptor } from './Descriptors/SsdpDescriptor';
import { MessageFactory } from './Messages/MessageFactory';
import { SsdpSocket } from './Sockets/SsdpSocket';
import { SsdpConfig } from './SsdpConfig';
import { SsdpConfigLoader } from './SsdpConfigLoader';
import { getNamedLogger, LOGGER_NAME } from './tools';

const logger = getNamedLogger(LOGGER_NAME);

export class SsdpAdvertiser implements IDisposable {
  protected socket: SsdpSocket;
  readonly ssdpConfig: SsdpConfig;
  private timer: NodeJS.Timeout;

  constructor(socket: SsdpSocket, ssdpConfig: SsdpConfig) {
    this.socket = socket;
    this.ssdpConfig = ssdpConfig;
  }

  startPublishing(): void {
    if (this.timer !== undefined) {
      return;
    }

    logger.info('Start publishing');

    // this.socket.on(SsdpSocketEvents.msearch, (m, r) => this.searchResponse(m, r));

    this.sendDiscoveryMessages();

    const interval = (this.ssdpConfig.SsdpAdvertiser.CacheControl / 3) * 1000;
    this.timer = setInterval(
      () => {
        this.sendDiscoveryMessages();
      },
      interval
    );
  }

  async stopPublishing(): Promise<void> {
    if (this.timer !== undefined) {
      clearTimeout(this.timer);
      this.timer = undefined;
    }
    // this.socket.removeListener(SsdpSocketEvents.msearch, this.searchResponse);
    await this.sendByebyeMessages();
    logger.info('Stop publishing');
  }

  private sendDiscoveryMessages(): void {
    logger.debug('sendDiscoveryMessages');
    const factory = new MessageFactory(this.ssdpConfig);

    const discoveryMessages = this.createMessages((ssdpDescriptor) =>
      factory.createAliveMessage(ssdpDescriptor)
    );

    discoveryMessages.forEach(async m => {
      await sleep(100);
      this.publish(m);
    });
  }

  private async sendByebyeMessages(): Promise<void> {
    logger.debug('sendByebyeMessages');
    const factory = new MessageFactory(this.ssdpConfig);

    const discoveryMessages = this.createMessages((ssdpDescriptor) =>
      factory.createByebyeMessage(ssdpDescriptor)
    );

    for (const m of discoveryMessages) {
      await sleep(50);
      this.publish(m);
    }
  }

  createMessages(messageFactory: (ssdpDescriptor: SsdpDescriptor) => string[]): string[] {
    const discoveryMessages: string[] = [];

    // Load config
    const rootDevice = SsdpConfigLoader.createRootDeviceDescriptor(this.ssdpConfig);

    const rootItems = messageFactory(rootDevice);
    discoveryMessages.push(...rootItems);

    let items = rootDevice.services.map(s => messageFactory(s));
    if (items.length > 0) {
      discoveryMessages.push(...items.reduce((p, c) => p.concat(c)));
    }

    items = rootDevice.devices.map(d => {
      const devices = messageFactory(d);
      const deviceServices = d.services.map(ds => messageFactory(ds));

      return devices.concat(...deviceServices);
    });
    if (items.length > 0) {
      discoveryMessages.push(...items.reduce((p, c) => p.concat(c)));
    }

    return discoveryMessages;
  }

  publish(message: string): void {
    this.socket.publish(message);
  }

  // private searchResponse(message: SsdpSearchMessage, rinfo: RemoteInfo): void {
  //   switch (message.ST) {
  //     case SsdpConstants.upnpRootDevice:
  //     case SsdpConstants.ssdpAll:
  //       const factory = new MessageFactory(this.ssdpConfig);
  //       const discoveryMessages = this.createMessages((ssdpDescriptor) =>
  //         factory.createAliveMessage(ssdpDescriptor)
  //       );
  //       const socket = createSocket(
  //         {
  //           type: rinfo.family === 'IPv4' ? 'udp4' : 'udp6'

  //         }
  //       );

  //       discoveryMessages.forEach(response => {
  //         socket.send(response, 1900, rinfo.address, (error, bytes) => {
  //           if (error !== undefined && error !== null) {
  //             logger.error(error);
  //           }
  //         });
  //       });
  //       socket.close();

  //   }
  // }

  async dispose(): Promise<void> {
    if (this.socket === undefined) {
      return;
    }

    await this.stopPublishing();
    this.socket = undefined;
  }

}
