export enum SsdpType {
  rootDevice = 'rootdevice',
  device = 'device',
  service = 'service'
}
