import { SsdpDeviceDescriptor } from './SsdpDeviceDescriptor';
import { SsdpType } from './SsdpType';

export class SsdpRootDeviceDescriptor extends SsdpDeviceDescriptor {
  readonly devices: SsdpDeviceDescriptor[] = [];

  constructor(domain: string, name: string, uuid: string) {
    super(name);
    this.domain = domain;
    this.type = SsdpType.rootDevice;
    this.uuid = uuid;
  }

  addDevice(device: SsdpDeviceDescriptor) {
    if (device.location === undefined) {
      device.location = this.location;
    }

    this.devices.push(device);
  }

  getUSN(index: number): string {
    switch (index) {
      case 1:
        return `uuid:${this.uuid}::upnp:rootdevice`;

      case 2:
        return `uuid:${this.uuid}`;

      case 3:
        return `uuid:${this.uuid}::urn:${this.domain}:device:${this.name}`;

    }
  }

  getNT(index: number): string {
    switch (index) {
      case 1:
        return `upnp:rootdevice`;

      case 2:
        return `uuid:${this.uuid}`;

      case 3:
        // urn:domain-name:device:deviceType:ver
        return `urn:${this.domain}:device:${this.name}`;

    }
  }

}
