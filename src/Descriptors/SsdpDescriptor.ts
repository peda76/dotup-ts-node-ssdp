import { SsdpType } from './SsdpType';
import { KeyValuePair } from 'dotup-ts-types';

export abstract class SsdpDescriptor {
  readonly name: string;
  domain: string;
  location: string;
  uuid: string;
  type: SsdpType;
  header: KeyValuePair<string, string>[];

  abstract getUSN(index: number): string;
  abstract getNT(index: number): string;

  constructor(name: string) {
    // this.domain = domain;
    this.name = name;
    this.header = [];
  }

  addHeader(key: string, value: string): void {
    this.header.push(
      {
        key: key,
        value: value
      }
    );
  }

}
