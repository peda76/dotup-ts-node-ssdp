import { SsdpDescriptor } from './SsdpDescriptor';
import { SsdpServiceDescriptor } from './SsdpServiceDescriptor';
import { SsdpType } from './SsdpType';

export class SsdpDeviceDescriptor extends SsdpDescriptor {
  readonly services: SsdpServiceDescriptor[] = [];

  constructor(name: string) {
    super(name);
    this.type = SsdpType.device;
  }

  addService(service: string | SsdpServiceDescriptor) {
    let newService: SsdpServiceDescriptor;
    if (typeof service === 'string') {
      newService = new SsdpServiceDescriptor(service);
      newService.domain = this.domain;
      newService.type = SsdpType.service;
    } else {
      newService = service;
    }
    newService.uuid = this.uuid;
    if (newService.domain === undefined) {
      newService.domain = this.domain;
    }
    if (newService.location === undefined) {
      newService.location = this.location;
    }
    this.services.push(newService);
  }

  // uuid:device-UUID::urn:domain-name:device:deviceType:ver
  getUSN(index: number): string {
    switch (index) {
      case 1:
        return `uuid:${this.uuid}`;

      case 2:
        return `uuid:${this.uuid}::urn:${this.domain}:device:${this.name}`;

    }
  }

  getNT(index: number): string {
    switch (index) {
      case 1:
        return `uuid:${this.uuid}`;

      case 2:
        // urn:domain-name:device:deviceType:ver
        return `urn:${this.domain}:device:${this.name}`;

    }
  }


}
