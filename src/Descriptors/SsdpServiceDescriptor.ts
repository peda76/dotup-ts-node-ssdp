import { SsdpDescriptor } from './SsdpDescriptor';
import { SsdpRootDeviceDescriptor } from './SsdpRootDeviceDescriptor';
import { SsdpType } from './SsdpType';

export class SsdpServiceDescriptor extends SsdpDescriptor {
  readonly services: SsdpServiceDescriptor[] = [];

  constructor(name: string) {
    super(name);
    this.type = SsdpType.service;
  }


  // uuid:device-UUID::urn:domain-name:service:serviceType:ver
  getUSN(index: number): string {
    return `uuid:${this.uuid}::urn:${this.domain}:service:${this.name}`;
  }

  getNT(index: number): string {
    return `urn:${this.domain}:service:${this.name}`;
  }
}
