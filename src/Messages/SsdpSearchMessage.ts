
/**
 *   NOTIFY * HTTP/1.1
HOST: 239.255.255.250: 1900
CACHE - CONTROL: max - age = seconds until advertisement expires
LOCATION: URL for UPnP description for root device
NT: notification type
NTS: ssdp: alive
SERVER: OS / version UPnP / 1.1 product / version
USN: composite identifier for the advertisement
BOOTID.UPNP.ORG: number increased each time device sends an initial announce or an update
message
CONFIGID.UPNP.ORG: number used for caching description information
SEARCHPORT.UPNP.ORG: number identifies port on which device responds to unicast M - SEARCH

 */

export class SsdpSearchMessage {
  HOST: string;
  ST: string;
  Man: string;
  MX: string;
}
