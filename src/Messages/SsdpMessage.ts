
/**
 *   NOTIFY * HTTP/1.1
HOST: 239.255.255.250: 1900
CACHE - CONTROL: max - age = seconds until advertisement expires
LOCATION: URL for UPnP description for root device
NT: notification type
NTS: ssdp: alive
SERVER: OS / version UPnP / 1.1 product / version
USN: composite identifier for the advertisement
BOOTID.UPNP.ORG: number increased each time device sends an initial announce or an update
message
CONFIGID.UPNP.ORG: number used for caching description information
SEARCHPORT.UPNP.ORG: number identifies port on which device responds to unicast M - SEARCH

 */
export class SsdpMessage {
  NTS: string;
  /**
   * A notification type (e.g., device type), sent in an NT (Notification Type) header field.
   */
  NT: string;
  /**
   * A composite identifier for the advertisement, sent in a USN (Unique Service Name) header field.
   *
   * REQUIRED. Field value contains Unique Service Name. Identifies a unique instance of a device or service. MUST be one of
   * the following. (See Table 1-1, “Root device discovery messages”, Table 1-2, “Embedded device discovery messages”,
   * and Table 1-3, “Service discovery messages” above.) The prefix (before the double colon) MUST match the value of the U
   * element in the device description. (Section DN2, “Description” explains the UDN element.) Single URI.
   */
  USN: string;
  /**
   * A URL for more information about the device (or enclosing device in the case of a service), sent in a LOCATION header field.
   */
  LOCATION: string;
  // A duration for which the advertisement is valid, sent in a CACHE-CONTROL header field.
  'CACHE-CONTROL': string;

}
