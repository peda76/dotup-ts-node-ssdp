import { SsdpDescriptor } from '../Descriptors/SsdpDescriptor';
import { SsdpType } from '../Descriptors/SsdpType';
import { SsdpConfig } from '../SsdpConfig';
import { SsdpConstants } from '../SsdpConstants';
import { getOsAndVersion } from '../tools';

export class MessageFactory {
  readonly config: SsdpConfig;

  constructor(config: SsdpConfig) {
    this.config = config;
  }

  createAliveMessage(ssdpDescriptor: SsdpDescriptor): string[] {
    let messageNumbers = 0;
    const result: string[] = []; // new DiscoveryMessage();
    const base: string[] = [];
    const cacheControlSec = this.config.SsdpAdvertiser.CacheControl;

    base.push(SsdpConstants.StartLine.NOTIFY);
    base.push(`NTS: ${SsdpConstants.ssdpAlive}`);
    base.push(`HOST: ${SsdpConstants.ipv4Address}`);
    base.push(`CACHE-CONTROL: max-age=${cacheControlSec}`);
    base.push(`SERVER: ${getOsAndVersion()} UPnP/1.1 ${this.config.ProductName}/${this.config.ProductVersion}`);
    // SERVER: unix/5.1 MyProduct/1.0
    switch (ssdpDescriptor.type) {
      case SsdpType.rootDevice:
        messageNumbers = 3;
        break;

      case SsdpType.device:
        messageNumbers = 2;
        break;

      case SsdpType.service:
        messageNumbers = 1;

    }

    for (let index = 1; index <= (messageNumbers); index += 1) {
      const copy = base.slice();
      copy.push(`NT: ${ssdpDescriptor.getNT(index)}`);
      copy.push(`USN: ${ssdpDescriptor.getUSN(index)}`);
      copy.push(`LOCATION: ${ssdpDescriptor.location}`);
      ssdpDescriptor.header.forEach(h => copy.push(`${h.key}: ${h.value}`));
      copy.push('');
      result.push(copy.join('\r\n'));
    }

    return result;
  }

  createByebyeMessage(ssdpDescriptor: SsdpDescriptor): string[] {
    let messageNumbers = 0;
    const result: string[] = []; // new DiscoveryMessage();

    const base: string[] = [];
    base.push(SsdpConstants.StartLine.NOTIFY);
    base.push(`NTS: ${SsdpConstants.ssdpByebye}`);
    base.push(`HOST: ${SsdpConstants.ipv4Address}`);

    switch (ssdpDescriptor.type) {
      case SsdpType.rootDevice:
        messageNumbers = 3;
        break;

      case SsdpType.device:
        messageNumbers = 2;
        break;

      case SsdpType.service:
        messageNumbers = 1;

    }

    for (let index = 1; index <= (messageNumbers); index += 1) {
      const copy = base.slice();
      copy.push(`NT: ${ssdpDescriptor.getNT(index)}`);
      copy.push(`USN: ${ssdpDescriptor.getUSN(index)}`);
      copy.push('');
      result.push(copy.join('\r\n'));
    }

    return result;
  }

}
