import { IDisposable } from 'dotup-ts-types';
import { EventEmitter } from 'events';
import { setInterval } from 'timers';
import { SsdpType } from './Descriptors/SsdpType';
import { SsdpMessage } from './Messages/SsdpMessage';
import { SsdpSocket } from './Sockets/SsdpSocket';
import { SsdpSocketEvents } from './Sockets/SsdpSocketEvents';
import { SsdpConstants } from './SsdpConstants';
import { SsdpControlPointEvents } from './SsdpControlPointEvents';
import { SsdpItem } from './SsdpItem';

export class SsdpControlPoint extends EventEmitter implements IDisposable {
  private readonly socket: SsdpSocket;
  private readonly items: SsdpItem[];
  timer: NodeJS.Timeout;

  constructor(socket: SsdpSocket) {
    super();
    this.socket = socket;
    this.items = [];
  }

  initialize(): void {
    this.socket.on(SsdpSocketEvents.notify, msg => this.onNotify(msg));

    this.timer = setInterval(
      () => {
        this.cleanUp();
      },
      1000
    );
  }

  onNotify(msg: SsdpMessage): void {
    switch (msg.NTS) {

      case SsdpConstants.ssdpAlive:
        this.addOrUpdateItem(msg);
        break;

      case SsdpConstants.ssdpByebye:
        this.removeItem(msg);

    }
  }

  removeItem(msg: SsdpMessage | SsdpItem) {
    const item = msg instanceof SsdpMessage ? this.parseUsn(msg) : msg;

    if (item !== undefined) {
      const index = this.items.indexOf(item);
      if (index >= 0) {
        this.items.splice(index, 1);
        this.emit(SsdpControlPointEvents.lost, item);
      }
    }
  }

  addOrUpdateItem(msg: SsdpMessage) {
    const item = this.parseUsn(msg);

    if (item === undefined) {
      return;
    }

    const index = this.items.findIndex(x =>
      x.uuid === item.uuid &&
      x.domain === item.domain &&
      x.name === item.name &&
      x.type === item.type
    );

    if (index < 0) {
      this.items.push(item);
      this.emit(SsdpControlPointEvents.found, item, msg);
    } else {
      const entry = this.items[index];
      entry.timestamp = item.timestamp;
    }
  }

  // 'USN: uuid:9999::urn:dotup:device:motobox'
  // 'USN: uuid:9999::urn:dotup:service:mqtt'
  parseUsn(msg: SsdpMessage): SsdpItem {
    const usn = msg.USN.split('::');

    if (usn.length < 2) {
      return undefined;
    }

    let uuid = usn[0];
    uuid = uuid
      .substr(uuid.lastIndexOf(':') + 1)
      .trim();

    const urn = usn[1].split(':');

    if (urn.length < 3) {
      return undefined;
    }

    const validity = msg['CACHE-CONTROL'] === undefined ? 0 : Number(msg['CACHE-CONTROL'].split('=')[1]);

    return {
      uuid: uuid,
      domain: urn[1],
      type: <SsdpType>urn[2],
      name: urn[3],
      timestamp: new Date().toUTCString(),
      validitySEC: validity,
      USN: msg.USN
    };
  }
  // discover(): void {

  // }

  cleanUp() {
    const remove: SsdpItem[] = [];
    this.items.forEach(item => {
      const currentDate = new Date(new Date().toUTCString());
      const itemDate = new Date(item.timestamp);
      const diff = currentDate.getUTCSeconds() - itemDate.getUTCSeconds();
      const valid = Number(item.validitySEC);
      if (diff > (valid + (valid / 2))) {
        // Invalid
        remove.push(item);
      }
    });

    remove.forEach(item => {
      this.removeItem(item);
    });
  }

  search(): void {
    throw new Error('not implemented');
  }

  on<T extends SsdpMessage>(event: SsdpControlPointEvents.found, listener: (item: SsdpItem, message: T) => void): this;
  on(event: SsdpControlPointEvents.lost, listener: (item: SsdpItem) => void): this;
  // tslint:disable-next-line: no-unnecessary-override : no-any
  on(event: string | symbol, listener: (...args: any[]) => void): this {
    return super.on(event, listener);
  }

  dispose(): void {
    this.socket.off(SsdpSocketEvents.notify, msg => this.onNotify(msg));
    if (this.timer !== undefined) {
      clearInterval(this.timer);
    }
  }

}
