
export namespace SsdpConstants {
  export enum HeaderKeys {
    NT = 'NT',
    USN = 'USN',
    LOCATION = 'LOCATION',
    CACHE_CONTROL = 'CACHE-CONTROL'
  }

  export enum StartLine {
    'NOTIFY' = 'NOTIFY * HTTP/1.1',
    'M-SEARCH' = 'M-SEARCH * HTTP/1.1',
    'HTTP' = 'HTTP/1.1 200 OK'
    /**
     * NOTIFY * HTTP/1.1\r\n
     *
        M-SEARCH * HTTP/1.1\r\n
        HTTP/1.1 200 OK\r\n
     */
  }
  export const ipv6AddressLinkLocal: string = 'FF02::C';
  export const ipv4Address: string = '239.255.255.250';
  export const port: number = 1900;
  export const ttl: number = 2;

  export const mSearch: string = 'M-SEARCH';
  export const ssdpAll: string = 'ssdp:all';
  export const ssdpDiscover: string = 'ssdp:discover';
  export const ssdpAlive: string = 'ssdp:alive';
  export const ssdpByebye: string = 'ssdp:byebye';

  // Upnp
  export const upnpRootDevice: string = 'upnp:rootdevice';
  export const upnpBasicDevice: string = 'Basic';
  export const pnpRootDevice: string = 'pnp:rootdevice';
}
