// tslint:disable: interface-name
import { StringProperty } from './tools';

export interface SsdpConfig {
  ProductName: string;
  ProductVersion: string;
  Interfaces?: string[];
  SsdpAdvertiser?: SsdpAdvertiserConfig;
  // LogLevel: string[];
}

export interface SsdpAdvertiserConfig {
  CacheControl: number;
  RootDevice: Partial<DeviceConfig>;
}

export interface DeviceConfig {
  domain: string;
  name: string;
  services: ServiceConfig[];
}

export interface ServiceConfig {
  name: string;
  headers: StringProperty;
}
