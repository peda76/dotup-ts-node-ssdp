export class SsdpHeader {
  NTS: string;
  /**
   * A notification type (e.g., device type), sent in an NT (Notification Type) header field.
   */
  NT: string;
  /**
   * A composite identifier for the advertisement, sent in a USN (Unique Service Name) header field.
   * 
   * REQUIRED. Field value contains Unique Service Name. Identifies a unique instance of a device or service. MUST be one of
   * the following. (See Table 1-1, “Root device discovery messages”, Table 1-2, “Embedded device discovery messages”,
   * and Table 1-3, “Service discovery messages” above.) The prefix (before the double colon) MUST match the value of the U
   * element in the device description. (Section DN2, “Description” explains the UDN element.) Single URI.
   */
  USN: string;
  /**
   * A URL for more information about the device (or enclosing device in the case of a service), sent in a LOCATION header field.
   */
  LOCATION: string;
  // A duration for which the advertisement is valid, sent in a CACHE-CONTROL header field.
  'CACHE-CONTROL': string;
  // No SSDP Standard
  HOSTNAME: string;
  /**
   * REQUIRED. Field value contains multicast address and port reserved for SSDP by Internet Assigned Numbers Authority (IANA).
   * MUST be 239.255.255.250:1900. If the port number (“:1900”) is omitted, the receiver MUST assume the default SSDP port number of 1900.
   */
  HOST: string;
  /**
   * REQUIRED. Specified by UPnP vendor. String. Field value MUST begin with the following “product tokens” (defined by
   * HTTP/1.1). The first product token identifes the operating system in the form OS name/OS version, the second token
   * represents the UPnP version and MUST be UPnP/1.1, and the third token identifes the product using the form
   * product name/product version. For example, “SERVER: unix/5.1 UPnP/1.1 MyProduct/1.0”. Control points MUST be
   * prepared to accept a higher minor version number of the UPnP version than the control point itself implements. For
   * example, control points implementing UDA version 1.0 will be able to interoperate with devices implementing UDA version 1.1
   */
  SERVER: string;
  /**
REQUIRED. The BOOTID.UPNP.ORG header field represents the boot instance of the device expressed according to a
monotonically increasing value. Its field value MUST be a non-negative 31-bit integer; ASCII encoded, decimal, without
leading zeros (leading zeroes, if present, MUST be ignored by the recipient) that MUST be increased on each initial announce
of the UPnP device or MUST be the same as the field value of the NEXTBOOTID.UPNP.ORG header field in the last sent SSDP
update message. Its field value MUST remain the same on all periodically repeated announcements. A convenient
mechanism is to set this field value to the time that the device sends its initial announcement, expressed as seconds
elapsed since midnight January 1, 1970; for devices that have a notion of time, this will not require any additional state to
remember or be “flashed”. However, it is perfectly acceptable to use a simple boot counter that is incremented on every
initial announcement as a field value of this header field. As such, control points MUST NOT view this header field as a
timestamp. The BOOTID.UPNP.ORG header field MUST be included in all announcements of a root device, its embedded
devices and its services. Unless the device explicitly updates its value by sending an SSDP update message, as long as the
device remains available in the network, the same BOOTID.UPNP.ORG field value MUST be used in all announcements,
search responses, update messages and eventually bye-bye messages.
Control points can use this header field to detect the case when a device leaves and rejoins the network (“reboots” in UPnP
terms). It can be used by control points for a number of purposes such as re-establishing desired event subscriptions,
checking for changes to the device state that were not evented since the device was off-line.
   */
  BOOTID: string;
  /**
   * REQUIRED. The CONFIGID.UPNP.ORG field value MUST be a non-negative, 31-bit integer, ASCII encoded, decimal, without
leading zeros (leading zeroes, if present, MUST be ignored by the recipient) that MUST represent the configuration number
of a root device. UPnP 1.1 devices MAY freely assign configid numbers from 0 to 16777215 (2^24-1). Higher numbers are
reserved for future use, and can be assigned by the Technical Committee. The configuration of a root device consists of the
following information: the DDD of the root device and all its embedded devices, and the SCPDs of all the contained services.
If any part of the configuration changes, the CONFIGID.UPNP.ORG field value MUST be changed. The CONFIGID.UPNP.ORG
header field MUST be included in all announcements of a root device, its embedded devices and its services. The
configuration number that is present in a CONFIGID.UPNP.ORG field value MUST satisfy the following rule:
- if a device sends out two messages with a CONFIGID.UPNP.ORG header field with the same field value K, the
configuration MUST be the same at the moments that these messages were sent.
Whenever a control point receives a CONFIGID.UPNP.ORG header field with a field value K, and subsequently downloads the
configuration information, this configuration information is associated with K. As an additional safeguard, the device MUST
include a configId attribute with value K in the returned description (see section 2, “Description”). The following caching
rules for control points supersede the caching rules that are defined in UPnP 1.0:
- Control points MAY ignore the CONFIGID.UPNP.ORG header field and use the caching rules that are based on
advertisement expirations as defined in Chapter 2, Description: as long as at least one of the discovery advertisements
from a root device, its embedded devices and its services have not expired, a control point MAY assume that the root
device and all its embedded devices and all its services are available. The device and service descriptions MAY be
retrieved at any point since the device and service descriptions are static as long as the device and its services are
available.
- If no configuration number is included in a received SSDP message, control points SHOULD cache based on
advertisement expirations as defined in Chapter 2 Description.
- If a CONFIGID.UPNP.ORG header field with field value K is included in a received SSDP message, and a control point has
already cached information associated with field value K, the control point MAY use this cached information as the
current configuration of the device. Otherwise, a control point SHOULD assume it has not cached the current
configuration of the device and needs to send new description query messages.
The CONFIGID.UPNP.ORG header field reduces peak loads on UPnP devices during startup and during network hiccups. Only
if a control point receives an announcement of an unknown configuration is downloading required.
 */
  CONFIGID: string;
  /**
   * OPTIONAL. If a device does not send the SEARCHPORT.UPNP.ORG header field, it MUST respond to unicast M-SEARCH
messages on port 1900. Only if port 1900 is unavailable MAY a device select a different port to respond to unicast M-SEARCH
messages. If a device sends the SEARCHPORT.UPNP.ORG header field, its field value MUST be an ASCII encoded integer,
decimal, without leading zeros (leading zeroes, if present, MUST be ignored by the recipient), in the range 49152-65535
(RFC 4340). The device MUST respond to unicast M-SEARCH messages that are sent to the advertised port.
   */
  SEARCHPORT?: string;
}
